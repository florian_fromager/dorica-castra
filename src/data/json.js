const Json = [
  {
    title: "dorica castra",
    content: `
      dorica castra* est une structure éditoriale protocolaire créée par [contact](marine montagné et timothée villemur), qui fonctionne selon la logique suivante
      → chaque publication, réalisée en collaboration avec un.e invité.e, est le maillon d’une chaîne de productions éditées
      → la thématique de chacune d’entre elles est tirée de la publication précédente
      → de chaque édition en découle une suivante, qui en déclenchera une autre, et ainsi de suite
      → chaque titre reprend la fin du titre du livre précédent, comme dans le «&#8239;jeu des queues&#8239;»

      ↷ dans [Manuel](Manuel) qui est le livre déclencheur de dorica castra, nous avons travaillé sur la manipulation du livre, en poussant à l’extrême ses possibilités d’utilisation, allant jusqu’à le dégrader

      ↷ en suivant les logiques de fonctionnement de dorica castra, nous avons extrait de [Manuel](Manuel) la thématique de la destruction du livre
      → nous avons développé cette thématique dans notre première publication [ellipses](ellipses) en compagnie de l’artiste ismail alaoui fdili
      → [ellipses](ellipses) présente une fiction retracée par des artefacts postiches
      → à travers des reçus et des coupures de presse, nous suivons les derniers instants d’un auteur qui emporte avec lui son œuvre

      ↷ à partir de ce livre nous avons choisi de nous pencher sur l’idée de la charge narrative et symbolique des objets dans un récit
      → nous sommes actuellement en train de travailler sur cette [à paraître](nouvelle matière avec l’artiste et commissaire céline furet)`,
    images: [],
  },
  {
    title: "Manuel",
    content: `dans Manuel qui est le livre déclencheur de [dorica castra](dorica castra*), nous avons travaillé sur la manipulation du livre en poussant à l’extrême ses possibilités d’utilisation, allant jusqu’à le dégrader
      → Manuel est son propre manuel d’utilisation dans lequel sont énumérées soixante-douze actions réalisables avec lui-même
      → en suivant les logiques de fonctionnement de dorica castra, nous avons extrait de Manuel la thématique de la destruction du livre
      
      ↷ nous avons développé cette thématique dans notre première publication [ellipses](ellipses), en compagnie de l’artiste ismail alaoui fdili
      
      ↷ de [ellipses](ellipses) nous avons tiré l’idée de la charge narrative et symbolique des objets dans un récit
      → nous sommes en train de travailler sur cette [à paraître](nouvelle matière avec l’artiste et commissaire céline furet)
      
      ↷ ces publications sont issues de la structure éditoriale protocolaire dorica castra, créée par [contact](marine montagné et timothée villemur)
      
      ↷ n’hésitez pas à nous contacter <a href="mailto:doricastra@gmail.com">par e-mail</a> si vous souhaitez vous procurer un exemplaire de l’une de nos publications`,
    images: [
      "manuel-01",
      "manuel-02",
      "manuel-03",
      "manuel-04",
      "manuel-05",
    ],
  },
  {
    title: "ellipses",
    content: `ellipses est la première publication issue du protocole éditorial de [dorica castra](dorica castra*)
      
      ↷ nous avons choisi ici d’extraire de [Manuel](Manuel), notre livre déclencheur, la thématique de la destruction du livre
      
      ↷ nous l’avons développée en compagnie de l’artiste ismail alaoui fdili
      → ellipses présente une fiction retracée par des artefacts postiches
      → à travers des reçus et des coupures de presse, nous suivons les derniers instants d’un auteur qui emporte avec lui son œuvre
      → le livre prend la forme d’une enquête
      → son scénario est présenté dans un feuillet dissimulé à la fin du livre et permet d’éclaircir le déroulé des évènements
      → les différents éléments qui constituent le livre peuvent être décomposés et donne la possibilité au lecteur de reconfigurer la fiction
      
      ↷ à partir de ce livre nous avons choisi de nous pencher sur l’idée de la charge narrative et symbolique des objets dans un récit
      → nous sommes actuellement en train de travailler sur cette [à paraître](nouvelle matière avec l’artiste et commissaire céline furet)
      
      ↷ ces publications sont issues de la structure éditoriale protocolaire dorica castra, créée par [contact](marine montagné et timothée villemur)
      
      ↷ n’hésitez pas à nous contacter <a href="mailto:doricastra@gmail.com">par e-mail</a> si vous souhaitez vous procurer un exemplaire de l’une de nos publications`,
    images: [
      "ellipses-01",
      "ellipses-02",
      "ellipses-03",
      "ellipses-04",
      "ellipses-05",
      "ellipses-06",
    ],
  },
  {
    title: "À paraître",
    content: `en nous basant sur [ellipses](ellipses) qui est la précédente publication de [dorica castra](dorica castra*)
      → faisant elle-même suite à [Manuel](Manuel)
      
      ↷ nous avons choisi de nous pencher sur l’idée de la charge narrative et symbolique des objets dans un récit et de développer cette thématique dans la prochaine publication de dorica castra
      → pour ce faire nous avons invité céline furet, artiste et commissaire, à travailler sur cette nouvelle matière avec nous
      
      ↷ les publications dont il est question ici sont issues de la structure éditoriale protocolaire dorica castra, créée par [contact](marine montagné et timothée villemur)
      
      ↷ n’hésitez pas à nous contacter <a href="mailto:doricastra@gmail.com">par e-mail</a> si vous souhaitez vous procurer un exemplaire de l’une de nos publications`,
    images: [],
  },
  {
    title: "contact",
    content: `[dorica castra](dorica castra*) est une structure éditoriale protocolaire créée par <a href="https://www.instagram.com/marine_montagne/" target="_blank" >marine montagné</a> et <a href="https://www.instagram.com/timvillemur/" target="_blank" >timothée villemur</a>
    
    ↷ ils sont designers graphique, artistes et éditeurs
    → ils ont suivi ensemble un parcours aux beaux-arts de toulouse (isdat) dans l’option design graphique, où ils ont entamé leurs réflexions sur le livre d’artiste
    → ils poursuivent leurs études à l’ensad en design graphique pour timothée, et à la cambre dans l’atelier design du livre et du papier pour marine
    → récemment diplômés (2018 et 2019) ils mettent en place fin 2019 cette structure éditoriale protocolaire nommée dorica castra, qui a pour ambition de mettre en résonance leurs réflexions sur le livre
    → il s’agit d’une association loi 1901
    
    ↷ à travers le médium du livre, nous tentons d’explorer les notions de protocole et de langage
    → nous utilisons le livre et l’édition en général comme un espace de monstration et de diffusion
    
    ↷ [Manuel](Manuel) est le déclencheur de dorica castra
    
    ↷ [ellipses](ellipses) en est la première publication
    → issue de Manuel
    → réalisée avec ismail alaoui fdili

    ↷ nous travaillons actuellement sur [à paraître](la prochaine publication) avec céline furet
    
    ↷ n’hésitez pas à nous contacter <a href="mailto:doricastra@gmail.com">par e-mail</a> si vous souhaitez vous procurer un exemplaire de l’une de nos publications ou si vous avez des questions sur dorica castra
    → vous pouvez aussi retrouver nos actualités sur <a href="https://www.instagram.com/dorica.castra" target="_blank">Instagram</a> et <a href="https://www.facebook.com/dorica.castra" target="_blank">Facebook</a>`,
    images: [],
  },
  {
    title: "marquee",
    content: ` le terme dorica castra définit un type d’anadiplose, le dorica castra a de particulier que ce n’est pas le dernier mot comme dans l’anadiplose classique, mais le dernier son d’une expression ou d’une phrase, qui est utilisé pour commencer la suivante →`,
  },
];

export { Json };
