const initOrder = ul => {

    let lis = ul.querySelectorAll( "li" );
    
    for( let i = 0; i < lis.length; i++ ) {

        lis[ i ].dataset.id = i;

    }

}, sortId = ul => {

    return [].map.call( ul.querySelectorAll( "li" ), el => {
    
        return el;
        
    }).sort( ( a, b ) => {
    
        let filter_a = parseInt( a.dataset.id ),
            filter_b = parseInt( b.dataset.id );
        
        return filter_a < filter_b ? -1 : ( filter_a > filter_b ? 1 : 0 );
        
    })

}, setOrder = ( e, ul ) => {

    const list = sortId( ul );

    for ( let i = 0; i < e.dataset.id; i++ ) {

        list[ i ].dataset.id = i + 1;

    }
    
    e.dataset.id = 0;

    setZ( ul );

    setLeft( ul );

}, setZ = ul => {

    let lis = ul.querySelectorAll( "li" );

    for ( let i = lis.length; i > 0; i-- ) {

        sortId( ul )[ i - 1 ].style.zIndex = i;

    }

}, setLeft = ul => {

    let lis = ul.querySelectorAll( "li" );

    for ( let i = 0; i < lis.length; i++ ) {

        let left = 0;

        for ( let j = 0; j < i; j++ ) {

            left = left + sortId( ul )[ j ].offsetWidth;

        }

        sortId( ul )[ i ].style.left = `${ left }px`;

    }

}

export { initOrder, sortId, setOrder, setZ, setLeft };