"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initMarquee = exports.launchMarquee = exports.removeFirst = exports.setMarquee = exports.setNew = exports.calcMultiple = void 0;

var q = function q(e) {
  return document.querySelector(e);
},
    qAll = function qAll(e) {
  return document.querySelectorAll(e);
};

var calcMultiple = function calcMultiple(marquee, pWidth) {
  var _width = marquee.offsetWidth;
  return {
    width: function width() {
      return _width;
    },
    multiple: function multiple() {
      return Math.round(_width / pWidth);
    },
    difference: function difference() {
      return Math.abs(_width - pWidth);
    }
  };
},
    setNew = function setNew(sentence, container, left) {
  var clone = sentence.cloneNode(true);
  clone.style.transform = "translate(".concat(left, "px)");
  container.appendChild(clone);
},
    setMarquee = function setMarquee(marquee, pWidth) {
  var multiple = calcMultiple(marquee, pWidth).multiple(),
      left = 0,
      sentences = qAll("#marquee > div p"),
      sentencesLength = sentences.length - 1;

  for (var i = 1; i <= multiple; i++) {
    left = left + pWidth;

    if (i > sentencesLength) {
      console.log("new sentence");
      setNew(left);
    }
  }
},
    removeFirst = function removeFirst() {
  var first = q("#marquee > div p:first-child");
  first.parentNode.removeChild(first);
},
    launchMarquee = function launchMarquee(marquee, container, sentence, pWidth, pHeight) {
  var count = 0,
      interval_id = window.setInterval("", 9999);

  for (var i = 1; i < interval_id; i++) {
    window.clearInterval(i);
  }

  pWidth = sentence.offsetWidth;
  setInterval(function () {
    var sentences = qAll("#marquee > div p");

    for (var _i = 0; _i <= sentences.length - 1; _i++) {
      var left = pWidth * _i,
          pos = left - count,
          padding = _i * pHeight * 0.4;
      sentences[_i].style.transform = "translate( ".concat(pos + padding, "px )");
    }

    if (count === calcMultiple(marquee, pWidth).difference()) setNew(sentence, container, pWidth);

    if (count === pWidth) {
      count = 0;
      removeFirst();
    } else {
      count += 1;
    }
  }, 10);
},
    initMarquee = function initMarquee() {
  var marquee = q("#marquee");
  var container = q("#marquee > div");
  var sentence = q("#marquee > div p");
  var pWidth = sentence.offsetWidth;
  var pHeight = sentence.offsetHeight;
  setMarquee(marquee, pWidth);
  launchMarquee(marquee, container, sentence, pWidth, pHeight);
};

exports.initMarquee = initMarquee;
exports.launchMarquee = launchMarquee;
exports.removeFirst = removeFirst;
exports.setMarquee = setMarquee;
exports.setNew = setNew;
exports.calcMultiple = calcMultiple;